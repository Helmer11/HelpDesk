import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { InconvenienteComponent } from './inconveniente/inconveniente.component';
import { RegistroComponent } from './registro/registro.component';

const APP_Routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'inconveniente', component: InconvenienteComponent },
  { path: 'registro', component: RegistroComponent },
  {path: '**', pathMatch: 'full', redirectTo: 'login'}
];


export const APP_Router = RouterModule.forRoot(APP_Routes);



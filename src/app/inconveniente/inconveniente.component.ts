import { LoginComponent } from './../login/login.component';
 import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;


@Component({
    moduleId: module.id,
    selector: 'app-inconveniente',
    templateUrl: 'inconveniente.component.html'
})
export class InconvenienteComponent {
  
  public mensaje: string;

 constructor() {


}


public limpiar(f: NgForm) {
  f.reset();

}

  public EnviarData(form: NgForm) {

  console.log(form.value.email);

    // this.mensaje = 'Se ha enviado la solicitud de asistencia';
    swal('Mensaje Enviado!', 'Su solicitud de asistencia fue enviado!', 'success');
  }


}

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app-registro',
    templateUrl: 'registro.component.html',
    styleUrls: ['registro.component.scss']
})
export class RegistroComponent {
  usuario: string;
  email: string ;
  password: string;


constructor(private _router: Router) {
}

public Registrarse(usuario: string, email: string, password: string) {

    localStorage.setItem('usuario', usuario);
    localStorage.setItem('email', email );
    localStorage.setItem('password', password );


  this._router.navigate( ['/login']);

  console.log('PROBANDO REGISTRO');




}



}

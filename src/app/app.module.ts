 import { APP_Router } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import { FormsModule } from '@angular/forms';



// componentes de la aplicacion
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { InconvenienteComponent } from './inconveniente/inconveniente.component';
import { RegistroComponent } from './registro/registro.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InconvenienteComponent,
    RegistroComponent
  ],
  imports: [
    BrowserModule,
    APP_Router,
    FormsModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue : '/' }
  ],
  bootstrap: [AppComponent,
]
})
export class AppModule { }
